var tFranky = {
  updateTable: function (tableElJQ, table_data, myFunctions) {
    $(tableElJQ).html('');
    for (let index in table_data.thead) {
      if (index == 0) {
        $(tableElJQ).html('<thead><tr>' + "<th></th>".repeat(table_data.thead.length) + '</tr></thead>')
      }
      let el = table_data.thead[index];
      let last_th = $(tableElJQ).find('thead tr:eq(0) th').eq(index);
      if (typeof el == 'string') {
        last_th.html(el)
      } else if (typeof el == 'object') {
        if (el.html == undefined) {
          last_th.html(el.name);
        } else {
          last_th.html(el.html.replace(/{{name}}/, el.name));
        }
        if (el.class != undefined) {
          last_th.addClass(el.class);
        }
        if (el.data != undefined && typeof el.data == 'object') {
          for (let dataIndex in el.data) {
            last_th.attr('data-' + dataIndex, el.data[dataIndex]);
          }
        }
        if (el.attr != undefined && typeof el.attr == 'object') {

          for (let attrIndex in el.attr) {
            last_th.attr(attrIndex, el.attr[attrIndex]);
          }
        }
        if (el.f != undefined && typeof el.f == 'object') {
          if (el.f.name != undefined) {
            myFunctions[el.f.name](el, last_th)
          } else {
            for (let functionIndex in el.f) {
              myFunctions[el.f[functionIndex].name](el, last_th)
            }
          }
        }
      }
    }
    for (let indexTr in table_data.tbody.data) {
      let dataRow = table_data.tbody.data[indexTr];
      if (table_data.thead.length) {
        let tdstr = '<td></td>'.repeat(table_data.thead.length)
        if (indexTr > 0) {
          $(tableElJQ).find('tbody').append('<tr>' + tdstr + '</tr>');
        } else {
          $(tableElJQ).append('<tbody><tr>' + tdstr + '</tr></tbody>')
        }
        if (dataRow.val != undefined) {
          let this_tr = $(tableElJQ).find('tbody:eq(0) tr:eq(' + indexTr + ')');
          if (dataRow.primary != undefined) {
            this_tr.attr('data-primary', dataRow.primary)
          }
          if (dataRow.data != undefined) {
            for (let index in dataRow.data) {
              this_tr.data(index, dataRow.data[index]);
            }
          }

          if (dataRow.f != undefined && typeof  dataRow.f == 'object') {
            
            if (dataRow.f.name != undefined) {
              myFunctions[dataRow.f.name](dataRow, this_tr, indexTr, table_data)
            } else {
              for (let functionIndex in dataRow.f) {
                myFunctions[el.f[functionIndex].name](dataRow, this_tr, indexTr, table_data)
              }
            }
          }
          dataRow = dataRow.val;
        }
        
        for (let index = 0; index < table_data.thead.length; index++) {
          let el = dataRow[index];
          let this_td = $(tableElJQ).find('tbody:eq(0) tr:eq(' + indexTr + ') td').eq(index);
          if (table_data.tbody.buttons[index] != undefined) {
            let thisBtn = table_data.tbody.buttons[index];
            myFunctions[table_data.tbody.buttons[index].name](el, this_td, thisBtn)
            if (thisBtn.class != undefined && typeof thisBtn.class == "string") {
              let thisTdCalss = thisBtn.class.split(' ');
              for (let classIndex in thisTdCalss) {
                this_td.addClass(thisTdCalss[classIndex]);
              }
            }
          } else {
            if (typeof el == 'object' && el != null) {
              if (el.f != undefined && typeof el.f == 'object') {
                if (el.f.name != undefined) {
                  myFunctions[el.f.name](el, this_td, indexTr, table_data, index)
                } else {
                  for (let functionIndex in el.f) {
                    myFunctions[el.f[functionIndex].name](el, this_td, indexTr, table_data, index)
                  }
                }
              }
              if (el.html != undefined) {
                this_td.html(el.html)
              }
              if (el.class != undefined) {
                let thisTdCalss = el.class.split(' ');
                for (let classIndex in thisTdCalss) {
                  this_td.addClass(thisTdCalss[classIndex]);
                }
              }
              if (el.data != undefined && typeof el.data == 'object') {
                for (let dataIndex in el.data) {
                  this_td.attr('data-' + dataIndex, el.data[dataIndex]);
                }
              }
              if (el.attr != undefined && typeof el.attr == 'object') {

                for (let attrIndex in el.attr) {
                  this_td.attr(attrIndex, el.attr[attrIndex]);
                }
              }
            } else {
              this_td.html(el)
            }
          }
        }
      }
    }
  }
}